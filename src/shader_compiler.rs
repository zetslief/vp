use std::path::Path;

pub enum Stage {
    Fragment(&'static str),
    Vertex(&'static str),
}

impl Stage {
    pub fn parse(&self) -> (&'static str, &'_ Path) {
        match self {
            Stage::Fragment(path) => ("fragment", Path::new(path)),
            Stage::Vertex(path) => ("vertex", Path::new(path)),
        }
    }
}

pub struct ShaderCompiler {
    path_to_compiler_executable: std::path::PathBuf,
}

impl ShaderCompiler {
    pub fn new(path_to_compiler_executable: std::path::PathBuf) -> ShaderCompiler {
        ShaderCompiler { path_to_compiler_executable }
    }

    pub fn compile(&self, shader: Stage, output: &std::path::Path) -> bool {
        let (stage, path_to_shader) = shader.parse();
        let shader_file_type = match std::fs::metadata(path_to_shader) {
            Ok(metadata) => metadata.file_type(),
            Err(error) => {
                log::error!(
                    "Shader file does not exists {:?}: {:?}",
                    path_to_shader,
                    error
                );
                return false;
            }
        };
        if shader_file_type.is_dir() {
            log::error!("Shader file is a directory: {:?}", path_to_shader);
            return false;
        }
        let mut command = std::process::Command::new(&self.path_to_compiler_executable);
        command.arg(format!("-fshader-stage={stage}"));
        command.arg(path_to_shader);
        command.arg("-o");
        command.arg(output);
        log::info!("Compiling shader({path_to_shader:?}): {command:#?}");
        let mut child = match command.spawn() {
            Ok(child) => child,
            Err(error) => {
                log::error!("Failed to spawn compiler process: {error:?}");
                return false;
            }
        };
        match child.wait() {
            Ok(exit_status) => {
                if exit_status.success() {
                    true
                } else {
                    log::warn!("Shader compilation failed: {exit_status:?}");
                    false
                }
            }
            Err(error) => {
                log::error!("Failed to wait on compiler: {error:?}");
                return false;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_shader_compilation() {
        let glsl_fragment_shader = Stage::Fragment(Path::new(SHADER_FRAGMENT));
        let glsl_vertex_shader = Stage::Vertex(Path::new(SHADER_VERTEX));

        let spv_fragment_shader = Path::new(SHADER_FRAGMENT_OUTPUT);
        let spv_vertex_shader = Path::new(SHADER_VERTEX_OUTPUT);

        let shader_compiler = ShaderCompiler::new(std::path::PathBuf::from("glslc.exe"));

        assert!(
            shader_compiler.compile(&glsl_fragment_shader, &spv_fragment_shader),
            "Failed to compile fragment shader!"
        );
        assert!(
            shader_compiler.compile(&glsl_vertex_shader, &spv_vertex_shader),
            "Failed to compile vertex shader!"
        );
    }
}
