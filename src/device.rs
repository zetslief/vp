use ash;
use ash::vk;

pub struct Device {
    pub physical: vk::PhysicalDevice,
    pub logical: ash::Device,
    pub memory_properties: vk::PhysicalDeviceMemoryProperties,
    pub queue_family_indices: QueueFamilyIndices,
}

#[derive(Copy, Clone)]
pub struct QueueFamilyIndices {
    pub graphics_family: u32,
    pub presentation_family: u32,
}

impl Device {
    pub unsafe fn new(
        instance: &ash::Instance,
        surface: vk::SurfaceKHR,
        surface_loader: &ash::extensions::khr::Surface,
        enabled_layers: &[*const i8],
        device_extensions_layers: &[*const i8],
    ) -> Result<Option<Self>, vk::Result> {
        let (physical_device, queue_family_indices) =
            initialize_physical_device(instance, surface, surface_loader)
                .expect("Failed to init physical device");

        let logical_device = initialize_logical_device(
            &instance,
            physical_device,
            enabled_layers,
            &queue_family_indices,
            device_extensions_layers,
        )?;
        let memory_properties = instance.get_physical_device_memory_properties(physical_device);
        Ok(Some(Self {
            physical: physical_device,
            logical: logical_device,
            queue_family_indices,
            memory_properties,
        }))
    }
}

unsafe fn initialize_physical_device(
    instance: &ash::Instance,
    surface: vk::SurfaceKHR,
    surface_loader: &ash::extensions::khr::Surface,
) -> Option<(vk::PhysicalDevice, QueueFamilyIndices)> {
    let physical_devices = instance
        .enumerate_physical_devices()
        .expect("Physical devices are not available.");
    for device in physical_devices {
        let queue_family_indices = find_queue_families(instance, device, surface_loader, surface)
            .expect("Failed to find queue family indices.");
        log::debug!(
            "Device queue family indices: GRAPHICS_{:?}, PRESENTATION_{:?}",
            queue_family_indices.graphics_family,
            queue_family_indices.presentation_family
        );
        if is_device_suitable(instance, device) {
            return Some((device, queue_family_indices));
        }
    }
    None
}

fn find_queue_families(
    instance: &ash::Instance,
    device: vk::PhysicalDevice,
    surface_loader: &ash::extensions::khr::Surface,
    surface: vk::SurfaceKHR,
) -> Option<QueueFamilyIndices> {
    let device_queue_family_properties =
        unsafe { instance.get_physical_device_queue_family_properties(device) };
    let mut graphics_family_found = false;
    let mut presentation_family_found = false;
    let mut result: QueueFamilyIndices = QueueFamilyIndices {
        graphics_family: 0,
        presentation_family: 0,
    };
    log::trace!(
        "Found {} queue families!",
        device_queue_family_properties.len()
    );
    let mut index = 0;
    for device_queue_family_property in device_queue_family_properties {
        if device_queue_family_property
            .queue_flags
            .contains(vk::QueueFlags::GRAPHICS)
        {
            log::trace!("Graphics family found: {:?}", device_queue_family_property);
            graphics_family_found = true;
            result.graphics_family = index;
        }
        unsafe {
            match surface_loader.get_physical_device_surface_support(device, index, surface) {
                Ok(support) => {
                    if support {
                        log::trace!("Present family found: {:?}", device_queue_family_property);
                        presentation_family_found = true;
                        result.presentation_family = index;
                    }
                }
                Err(error) => {
                    log::error!("Error in finding present family index: {}", error);
                }
            };
        }
        index += 1;
    }
    if graphics_family_found && presentation_family_found {
        Some(result)
    } else {
        None
    }
}

fn is_device_suitable(instance: &ash::Instance, device: vk::PhysicalDevice) -> bool {
    let device_properties = unsafe { instance.get_physical_device_properties(device) };
    match device_properties.device_type {
        vk::PhysicalDeviceType::OTHER => false,
        device_type => {
            log::info!("Physical device of type {device_type:?} found!");
            let device_features = unsafe { instance.get_physical_device_features(device) };
            let geometry_shader_support = device_features.geometry_shader > 0;
            log::info!("Physical device support geometry shader: {geometry_shader_support}");
            geometry_shader_support
        }
    }
}

unsafe fn initialize_logical_device(
    instance: &ash::Instance,
    device: vk::PhysicalDevice,
    layer_names: &[*const i8],
    queue_family_indices: &QueueFamilyIndices,
    device_extension_names: &[*const i8],
) -> Result<ash::Device, vk::Result> {
    let graphics_family_index = queue_family_indices.graphics_family;
    let presentation_family_index = queue_family_indices.presentation_family;
    let mut device_queue_create_infos = Vec::<vk::DeviceQueueCreateInfo>::new();
    device_queue_create_infos.push(
        vk::DeviceQueueCreateInfo::builder()
            .queue_family_index(graphics_family_index)
            .queue_priorities(&[1.0])
            .build(),
    );
    if graphics_family_index != presentation_family_index {
        device_queue_create_infos.push(
            vk::DeviceQueueCreateInfo::builder()
                .queue_family_index(graphics_family_index)
                .queue_priorities(&[1.0])
                .build(),
        );
    }
    log::trace!("Device queue create info created!");
    let physical_device_features = instance.get_physical_device_features(device);
    let device_create_info = vk::DeviceCreateInfo::builder()
        .queue_create_infos(&device_queue_create_infos)
        .enabled_features(&physical_device_features)
        .enabled_layer_names(layer_names)
        .enabled_extension_names(device_extension_names);
    instance.create_device(device, &device_create_info, None)
}
