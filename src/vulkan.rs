use super::utils;
use crate::renderer::Renderer;
use crate::surface;
use crate::swapchain::{query_swap_chain_details, SwapchainSupportDetails};
use ash::extensions::khr;
use ash::vk;
use log;

use crate::device;

pub struct Vulkan {
    pub entry: ash::Entry,
    pub instance: ash::Instance,
    pub device: device::Device,
    pub surface: vk::SurfaceKHR,
    pub surface_loader: khr::Surface,
    pub swapchain_support_details: SwapchainSupportDetails,
}

impl Vulkan {
    pub unsafe fn cleanup(
        &mut self,
        renderer: &mut Renderer,
        shader_modules: Vec<vk::ShaderModule>,
        allocated_memory: Vec<(vk::Buffer, vk::DeviceMemory)>,
    ) {
        log::info!("Cleaning up vulkan instance");
        let logical = &self.device.logical;

        for pipeline in [renderer.graphics_pipeline, renderer.quad_pipeline] {
            logical.destroy_pipeline(pipeline, None);
        }

        for pipeline_cache in [renderer.pipeline_cache, renderer.quad_pipeline_cache] {
            logical.destroy_pipeline_cache(pipeline_cache, None);
        }

        logical.destroy_pipeline_layout(renderer.pipeline_layout, None);

        logical.destroy_descriptor_pool(renderer.descriptor_pool, None);
        logical.destroy_descriptor_set_layout(renderer.descriptor_set_layout, None);

        for framebuffer in renderer.framebuffers.iter() {
            logical.destroy_framebuffer(*framebuffer, None);
        }

        for image_view in renderer.image_views.iter() {
            logical.destroy_image_view(*image_view, None);
        }

        logical.destroy_render_pass(renderer.render_pass, None);

        for shader_module in shader_modules {
            logical.destroy_shader_module(shader_module, None);
        }

        for (buffer, device_memory) in allocated_memory {
            logical.destroy_buffer(buffer, None);
            logical.free_memory(device_memory, None);
        }

        renderer
            .swapchain_loader
            .destroy_swapchain(renderer.swapchain, None);

        logical.destroy_command_pool(renderer.command_pool, None);

        logical.destroy_semaphore(renderer.image_available_semaphore, None);
        logical.destroy_semaphore(renderer.render_finished_semaphore, None);

        logical.destroy_device(None);

        self.surface_loader.destroy_surface(self.surface, None);
        self.instance.destroy_instance(None)
    }
}

#[derive(Debug)]
pub enum VulkanInitializationError {
    Native(vk::Result),
    Custom(&'static str),
}

impl From<&'static str> for VulkanInitializationError {
    fn from(value: &'static str) -> Self {
        Self::Custom(value)
    }
}

impl From<vk::Result> for VulkanInitializationError {
    fn from(value: vk::Result) -> Self {
        Self::Native(value)
    }
}

impl Vulkan {
    pub unsafe fn new(
        display_handle: raw_window_handle::RawDisplayHandle,
        window_handle: raw_window_handle::RawWindowHandle,
        validation_layers: &[&str],
    ) -> Result<Self, VulkanInitializationError> {
        let entry = ash::Entry::linked();
        let application_info =
            create_application_info(&entry).expect("Application info initialized!");

        let layers = utils::strs_as_cstrings(&validation_layers);
        let extensions = utils::strings_as_cstrings(&enumerate_required_extensions(display_handle));

        if !are_layers_valid(&entry, &layers) {
            return Err("The layers are invalid for the Instance Create Info".into());
        }
        log::info!("The layers for the Instance Create Info are valid!");

        let enabled_layers = utils::cstrings_as_pointers(&layers);
        let enabled_extensions = utils::cstrings_as_pointers(&extensions);

        let instance_create_info =
            create_instance_create_info(&application_info, &enabled_layers, &enabled_extensions);

        let instance =
            create_vulkan_instance(&entry, &instance_create_info).expect("Instance initialized!");

        let surface = surface::create_surface(&instance, &entry, display_handle, window_handle)
            .expect("Surface can be initialized!");

        let surface_loader = khr::Surface::new(&entry, &instance);

        let device_extensions = [std::ffi::CString::from(ash::extensions::khr::Swapchain::name())];
        let device_extensions_raw = utils::cstrings_as_pointers(&device_extensions);

        let device = device::Device::new(
            &instance,
            surface.clone(),
            &surface_loader,
            &enabled_layers,
            &device_extensions_raw,
        )?
        .expect("Failed to find physical_device");

        if !is_swap_chain_available(&instance, device.physical, &device_extensions) {
            return Err("Swap chain is not available!".into());
        }

        let swapchain_support_details =
            query_swap_chain_details(&surface_loader, surface.clone(), device.physical)?;

        if !swapchain_support_details.is_swap_chain_supported() {
            return Err("Swap chain is not supported!".into());
        }

        Ok(Vulkan {
            entry,
            instance,
            device,
            surface,
            surface_loader,
            swapchain_support_details,
        })
    }
}

fn create_application_info(entry: &ash::Entry) -> Option<vk::ApplicationInfo> {
    let application_version = match entry.try_enumerate_instance_version().ok()? {
        Some(version) => vk::make_api_version(
            vk::api_version_variant(version),
            vk::api_version_major(version),
            vk::api_version_minor(version),
            vk::api_version_patch(version),
        ),
        None => vk::make_api_version(0, 1, 0, 0),
    };
    log::info!("Application version: {}", application_version);
    let result = Some(
        vk::ApplicationInfo::builder()
            .application_name(&std::ffi::CString::new("Vulkan Playground").unwrap())
            .engine_name(&std::ffi::CString::new("VP_ENGINE").unwrap())
            .application_version(application_version)
            .build(),
    );
    log::trace!("The Application Info is created!");
    return result;
}

fn create_instance_create_info(
    application_info: &vk::ApplicationInfo,
    layers: &[*const i8],
    extensions: &[*const i8],
) -> vk::InstanceCreateInfo {
    log::trace!("Creating the Instance Create Info...");
    let instance_create_info = vk::InstanceCreateInfo::builder()
        .application_info(application_info)
        .enabled_layer_names(layers)
        .enabled_extension_names(extensions)
        .build();
    log::trace!("The Instance Create Info is created!");
    instance_create_info
}

fn create_vulkan_instance(
    entry: &ash::Entry,
    instance_create_info: &vk::InstanceCreateInfo,
) -> Option<ash::Instance> {
    log::trace!("Creating an Vulkan instance...");
    unsafe {
        match entry.create_instance(&instance_create_info, None) {
            Ok(instance) => {
                log::info!("Instance created...");
                Some(instance)
            }
            Err(error) => {
                log::error!("Error while creating an instance: {}", error);
                None
            }
        }
    }
}

fn are_layers_valid(entry: &ash::Entry, layers: &[std::ffi::CString]) -> bool {
    let available_layers: Vec<_> = match entry.enumerate_instance_layer_properties() {
        Ok(layers) => layers
            .iter()
            .map(|layer| unsafe {
                std::ffi::CString::from(std::ffi::CStr::from_ptr(layer.layer_name.as_ptr()))
            })
            .collect(),
        Err(_) => {
            log::error!("Cannot find instance layer properties!");
            return false;
        }
    };
    log::trace!("Available layers: {available_layers:?}");
    layers.iter().all(move |layer| {
        match available_layers
            .iter()
            .find(|available_layer| available_layer == &layer)
        {
            Some(_value) => {
                log::info!("{:?} layer is available!", layer);
                true
            }
            None => {
                log::error!("{:?} layer is not available!", layer);
                false
            }
        }
    })
}

fn is_swap_chain_available(
    instance: &ash::Instance,
    device: vk::PhysicalDevice,
    extension_names: &[std::ffi::CString],
) -> bool {
    let device_extension_properties = unsafe {
        instance
            .enumerate_device_extension_properties(device)
            .expect("Cannot get device extension properties from instance!")
    };
    log::trace!("Device properties: {device_extension_properties:?}");
    let available_device_extensions: Vec<std::ffi::CString> = unsafe {
        device_extension_properties
            .iter()
            .map(|property| {
                std::ffi::CString::from(std::ffi::CStr::from_ptr(property.extension_name.as_ptr()))
            })
            .collect()
    };
    log::trace!("Available extensions: {available_device_extensions:?}");
    extension_names.iter().all(|required_extension| {
        available_device_extensions
            .iter()
            .any(|available_extension| available_extension == required_extension)
    })
}

fn enumerate_required_extensions(
    display_handle: raw_window_handle::RawDisplayHandle,
) -> std::vec::Vec<String> {
    match display_handle {
        #[cfg(target_os = "linux")]
        raw_window_handle::RawDisplayHandle::Xlib(_x_lib_handle) => vec![
            String::from(ash::extensions::khr::Surface::name().to_str().unwrap()),
            String::from(ash::extensions::khr::XlibSurface::name().to_str().unwrap()),
        ],
        #[cfg(target_os = "windows")]
        raw_window_handle::RawDisplayHandle::Windows(_windows_handle) => vec![
            String::from(ash::extensions::khr::Surface::name().to_str().unwrap()),
            String::from(ash::extensions::khr::Win32Surface::name().to_str().unwrap()),
        ],
        _ => vec![],
    }
}
