use log;

pub struct Logger;

impl Logger {
    pub fn init(&self, max_level: log::LevelFilter) {
        log::set_max_level(max_level)
    }
}

impl log::Log for Logger {
    fn enabled(&self, _metadata: &log::Metadata) -> bool {
        true
    }

    fn log(&self, record: &log::Record) {
        if self.enabled(record.metadata()) {
            println!("{} - {}", record.level(), record.args())
        }
    }

    fn flush(&self) {}
}
