use ash::vk;

pub unsafe fn draw(
    device: &ash::Device,
    framebuffer: vk::Framebuffer,
    command_pool: vk::CommandPool,
    command_buffer: vk::CommandBuffer,
    render_pass: vk::RenderPass,
    pipeline_layout: vk::PipelineLayout,
    descriptor_sets: &[vk::DescriptorSet],
    graphics_pipeline: vk::Pipeline,
    quad_pipeline: vk::Pipeline,
    default_buffers: &[vk::Buffer],
    vertex_count: usize,
    quad_buffers: &[vk::Buffer],
    quad_vertex_count: usize,
    swapchain_extent: vk::Extent2D,
) -> Result<(), vk::Result> {
    device.reset_command_pool(command_pool, vk::CommandPoolResetFlags::RELEASE_RESOURCES)?;
    let command_buffer_begin_info = vk::CommandBufferBeginInfo::builder().build();
    let clear_color = vk::ClearValue {
        color: vk::ClearColorValue {
            float32: [0.01f32, 0.01f32, 0.02f32, 1f32],
        },
    };
    let clear_values = [clear_color];
    let render_pass_begin_info = vk::RenderPassBeginInfo::builder()
        .render_pass(render_pass)
        .framebuffer(framebuffer)
        .render_area(vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent: swapchain_extent,
        })
        .clear_values(&clear_values)
        .build();
    device.begin_command_buffer(command_buffer, &command_buffer_begin_info)?;
    device.cmd_begin_render_pass(
        command_buffer,
        &render_pass_begin_info,
        vk::SubpassContents::INLINE,
    );

    device.cmd_bind_pipeline(
        command_buffer,
        vk::PipelineBindPoint::GRAPHICS,
        graphics_pipeline,
    );
    device.cmd_bind_descriptor_sets(
        command_buffer,
        vk::PipelineBindPoint::GRAPHICS,
        pipeline_layout,
        0,
        descriptor_sets,
        &[],
    );
    device.cmd_bind_vertex_buffers(command_buffer, 0, default_buffers, &[0]);

    device.cmd_draw(command_buffer, vertex_count as u32, 1, 0, 0);

    device.cmd_bind_pipeline(
        command_buffer,
        vk::PipelineBindPoint::GRAPHICS,
        quad_pipeline,
    );
    device.cmd_bind_vertex_buffers(command_buffer, 0, quad_buffers, &[0]);

    device.cmd_draw(command_buffer, quad_vertex_count as u32, 1, 0, 0);

    device.cmd_end_render_pass(command_buffer);

    device.end_command_buffer(command_buffer)
}
