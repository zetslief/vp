use crate::shaders::PipelineShaders;
use ash::vk;

pub struct GraphicsPipelineInfo {
    pub input_assembly: vk::PipelineInputAssemblyStateCreateInfo,
    pub viewport_state: vk::PipelineViewportStateCreateInfo,
    pub rasterizer: vk::PipelineRasterizationStateCreateInfo,
    pub multisampling: vk::PipelineMultisampleStateCreateInfo,
    pub color_blending: vk::PipelineColorBlendStateCreateInfo,
    pub viewports: Vec<vk::Viewport>,
    pub scissors: Vec<vk::Rect2D>,
    pub attachments: Vec<vk::PipelineColorBlendAttachmentState>,
}

impl GraphicsPipelineInfo {
    pub fn new(
        viewports: Vec<vk::Viewport>,
        scissors: Vec<vk::Rect2D>,
        attachments: Vec<vk::PipelineColorBlendAttachmentState>,
    ) -> Self {
        let input_assembly = vk::PipelineInputAssemblyStateCreateInfo::builder()
            .topology(vk::PrimitiveTopology::TRIANGLE_LIST)
            .primitive_restart_enable(false)
            .build();

        let viewport_state = vk::PipelineViewportStateCreateInfo::builder()
            .viewports(&viewports)
            .scissors(&scissors)
            .build();

        let rasterizer = vk::PipelineRasterizationStateCreateInfo::builder()
            .depth_clamp_enable(false)
            .rasterizer_discard_enable(false)
            .polygon_mode(vk::PolygonMode::FILL)
            .cull_mode(vk::CullModeFlags::BACK)
            .front_face(vk::FrontFace::CLOCKWISE)
            .depth_bias_enable(false)
            .build();

        let multisampling = vk::PipelineMultisampleStateCreateInfo::builder()
            .sample_shading_enable(false)
            .rasterization_samples(vk::SampleCountFlags::TYPE_1)
            .build();

        let color_blending = vk::PipelineColorBlendStateCreateInfo::builder()
            .logic_op_enable(false)
            .attachments(&attachments)
            .build();

        GraphicsPipelineInfo {
            input_assembly,
            viewport_state,
            rasterizer,
            multisampling,
            color_blending,
            viewports,
            scissors,
            attachments,
        }
    }
}

pub fn create_viewport(extent: vk::Extent2D) -> vk::Viewport {
    vk::Viewport::builder()
        .x(0f32)
        .y(0f32)
        .width(extent.width as f32)
        .height(extent.height as f32)
        .min_depth(0f32)
        .max_depth(1f32)
        .build()
}

pub fn create_scissor(extent: vk::Extent2D) -> vk::Rect2D {
    vk::Rect2D::builder()
        .offset(vk::Offset2D { x: 0, y: 0 })
        .extent(extent)
        .build()
}

pub fn create_color_blend_attachment() -> vk::PipelineColorBlendAttachmentState {
    vk::PipelineColorBlendAttachmentState::builder()
        .color_write_mask(
            vk::ColorComponentFlags::R
                | vk::ColorComponentFlags::G
                | vk::ColorComponentFlags::B
                | vk::ColorComponentFlags::A,
        )
        .blend_enable(false)
        .build()
}

pub unsafe fn create_graphics_pipeline(
    logical_device: &ash::Device,
    shaders: &PipelineShaders,
    info: GraphicsPipelineInfo,
    pipeline_layout: vk::PipelineLayout,
    render_pass: vk::RenderPass,
) -> Result<(Vec<vk::Pipeline>, vk::PipelineCache), vk::Result> {
    let pipeline_info = vk::GraphicsPipelineCreateInfo::builder()
        .stages(&shaders.shader_stages)
        .vertex_input_state(&shaders.pipeline_vertex_input_state)
        .input_assembly_state(&info.input_assembly)
        .viewport_state(&info.viewport_state)
        .rasterization_state(&info.rasterizer)
        .multisample_state(&info.multisampling)
        .color_blend_state(&info.color_blending)
        .layout(pipeline_layout)
        .render_pass(render_pass)
        .subpass(0)
        .build();

    let pipelines = vec![pipeline_info];
    let pipeline_cache_info = vk::PipelineCacheCreateInfo::builder().build();
    let pipeline_cache = logical_device.create_pipeline_cache(&pipeline_cache_info, None)?;
    Ok((
        logical_device
            .create_graphics_pipelines(pipeline_cache, &pipelines, None)
            .map_err(|(_, error)| error)?,
        pipeline_cache,
    ))
}
