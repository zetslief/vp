use super::pipeline::{
    create_color_blend_attachment, create_graphics_pipeline, create_scissor, create_viewport,
    GraphicsPipelineInfo,
};
use super::shaders::PipelineShaders;
use crate::device::QueueFamilyIndices;
use crate::renderer::Renderer;
use crate::vulkan::Vulkan;
use ash::vk;

pub struct SwapchainSupportDetails {
    pub capabilities: vk::SurfaceCapabilitiesKHR,
    pub formats: std::vec::Vec<vk::SurfaceFormatKHR>,
    pub presentation_modes: std::vec::Vec<vk::PresentModeKHR>,
}

impl SwapchainSupportDetails {
    pub fn is_swap_chain_supported(&self) -> bool {
        let formats_valid = self.formats.len() > 0;
        let presentation_modes_valid = self.presentation_modes.len() > 0;
        formats_valid && presentation_modes_valid
    }

    pub fn choose_surface_format(&self) -> vk::SurfaceFormatKHR {
        for available_format in self.formats.iter() {
            match (available_format.format, available_format.color_space) {
                (vk::Format::B8G8R8A8_SRGB, vk::ColorSpaceKHR::SRGB_NONLINEAR) => {
                    log::info!("Found perfect surface format: {:?}", available_format);
                    return available_format.clone();
                }
                (skipping_format, skipping_colorspace) => {
                    log::trace!(
                        "Skipping format: {:?}, color space: {:?}",
                        skipping_format,
                        skipping_colorspace
                    );
                }
            };
        }
        let result = self
            .formats
            .first()
            .expect("Formats are not empty!")
            .clone();
        log::warn!(
            "Cannot find perfect surface format! Selecting the first one: {:?}",
            result
        );
        result
    }

    pub fn choose_surface_presentation_mode(&self) -> vk::PresentModeKHR {
        for presentation_mode in self.presentation_modes.iter() {
            match presentation_mode {
                &vk::PresentModeKHR::MAILBOX => {
                    log::info!("Found perfect presentation mode: {:?}", presentation_mode);
                    return presentation_mode.clone();
                }
                _ => (),
            };
        }
        log::info!(
            "Choose default presentation mode: {:?}",
            vk::PresentModeKHR::FIFO
        );
        return vk::PresentModeKHR::FIFO;
    }

    pub fn choose_swap_extent(&self) -> vk::Extent2D {
        match self.capabilities.current_extent.width {
            u32::MAX => unimplemented!("Calculate valid image extent"),
            _ => self.capabilities.current_extent,
        }
    }
}

pub unsafe fn query_swap_chain_details(
    surface_loader: &ash::extensions::khr::Surface,
    surface: vk::SurfaceKHR,
    device: vk::PhysicalDevice,
) -> Result<SwapchainSupportDetails, vk::Result> {
    let capabilities = surface_loader.get_physical_device_surface_capabilities(device, surface)?;
    let formats = surface_loader.get_physical_device_surface_formats(device, surface)?;
    let presentation_modes =
        surface_loader.get_physical_device_surface_present_modes(device, surface)?;
    Ok(SwapchainSupportDetails {
        capabilities,
        formats,
        presentation_modes,
    })
}

pub unsafe fn create_swapchain(
    swap_chain_loader: ash::extensions::khr::Swapchain,
    surface: vk::SurfaceKHR,
    swap_chain_support_details: &SwapchainSupportDetails,
    queue_family_indices: &QueueFamilyIndices,
) -> Result<(vk::SwapchainKHR, u32), vk::Result> {
    let mut image_count = swap_chain_support_details.capabilities.min_image_count + 1;
    if swap_chain_support_details.capabilities.max_image_count > 0 {
        image_count = std::cmp::min(
            image_count,
            swap_chain_support_details.capabilities.max_image_count,
        );
    }

    let surface_format = swap_chain_support_details.choose_surface_format();
    let surface_presentation_mode = swap_chain_support_details.choose_surface_presentation_mode();
    let swap_extent = swap_chain_support_details.choose_swap_extent();

    let mut swap_chain_create_info_builder = vk::SwapchainCreateInfoKHR::builder()
        .surface(surface)
        .min_image_count(image_count)
        .image_format(surface_format.format)
        .image_color_space(surface_format.color_space)
        .image_extent(swap_extent)
        .image_array_layers(1)
        .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT);

    let indices = [queue_family_indices.graphics_family, queue_family_indices.presentation_family];
    if indices[0] != indices[1] {
        log::trace!("The graphics queue family is different from the presentation queue");
        swap_chain_create_info_builder = swap_chain_create_info_builder
            .image_sharing_mode(vk::SharingMode::CONCURRENT)
            .queue_family_indices(&indices);
    } else {
        log::trace!("The graphics queue family is the same as the presentation queue");
        swap_chain_create_info_builder =
            swap_chain_create_info_builder.image_sharing_mode(vk::SharingMode::EXCLUSIVE);
    }
    let swap_chain_create_info = swap_chain_create_info_builder
        .pre_transform(swap_chain_support_details.capabilities.current_transform)
        .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
        .present_mode(surface_presentation_mode)
        .clipped(true)
        .build();
    swap_chain_loader
        .create_swapchain(&swap_chain_create_info, None)
        .map(|swapchain| (swapchain, image_count))
}

pub unsafe fn create_image_views(
    swap_chain_loader: ash::extensions::khr::Swapchain,
    swap_chain: vk::SwapchainKHR,
    swap_chain_support_details: &SwapchainSupportDetails,
    device: &ash::Device,
) -> Result<Vec<vk::ImageView>, vk::Result> {
    let mut image_views = Vec::new();
    let swap_chain_images = swap_chain_loader.get_swapchain_images(swap_chain)?;
    for image in swap_chain_images.into_iter() {
        let image_view_create_info = vk::ImageViewCreateInfo::builder()
            .view_type(vk::ImageViewType::TYPE_2D)
            .image(image)
            .format(swap_chain_support_details.choose_surface_format().format)
            .components(
                vk::ComponentMapping::builder()
                    .r(vk::ComponentSwizzle::R)
                    .g(vk::ComponentSwizzle::G)
                    .b(vk::ComponentSwizzle::B)
                    .a(vk::ComponentSwizzle::A)
                    .build(),
            )
            .subresource_range(
                vk::ImageSubresourceRange::builder()
                    .aspect_mask(vk::ImageAspectFlags::COLOR)
                    .base_mip_level(0)
                    .level_count(1)
                    .base_array_layer(0)
                    .layer_count(1)
                    .build(),
            )
            .build();
        image_views.push(device.create_image_view(&image_view_create_info, None)?);
    }
    Ok(image_views)
}

pub unsafe fn create_render_pass(
    support_details: &SwapchainSupportDetails,
    logical_device: &ash::Device,
) -> Result<vk::RenderPass, vk::Result> {
    let color_attachment = vk::AttachmentDescription::builder()
        .format(support_details.choose_surface_format().format)
        .samples(vk::SampleCountFlags::TYPE_1)
        .load_op(vk::AttachmentLoadOp::CLEAR)
        .store_op(vk::AttachmentStoreOp::STORE)
        .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
        .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
        .initial_layout(vk::ImageLayout::UNDEFINED)
        .final_layout(vk::ImageLayout::PRESENT_SRC_KHR)
        .build();
    let color_attachment_ref = vk::AttachmentReference::builder()
        .attachment(0)
        .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
        .build();
    let references = vec![color_attachment_ref];
    let subpass = vk::SubpassDescription::builder()
        .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS)
        .color_attachments(&references)
        .build();

    let attachments = vec![color_attachment];
    let subpasses = vec![subpass];
    let subpass_dependency = vk::SubpassDependency::builder()
        .src_subpass(vk::SUBPASS_EXTERNAL)
        .dst_subpass(0)
        .src_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .src_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_READ) // ERROR MAY BE HERE
        .dst_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .dst_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_WRITE)
        .build();
    let dependencies = [subpass_dependency];

    let render_pass_info = vk::RenderPassCreateInfo::builder()
        .attachments(&attachments)
        .subpasses(&subpasses)
        .dependencies(&dependencies);
    logical_device.create_render_pass(&render_pass_info, None)
}

pub unsafe fn create_framebuffers(
    logical_device: &ash::Device,
    swapchain_extent: &vk::Extent2D,
    image_views: &[vk::ImageView],
    render_pass: vk::RenderPass,
) -> Result<Vec<vk::Framebuffer>, vk::Result> {
    let mut framebuffers = Vec::new();
    for image_view in image_views.iter() {
        let attachments = [*image_view];
        let framebuffer_create_info = vk::FramebufferCreateInfo::builder()
            .render_pass(render_pass)
            .attachments(&attachments)
            .width(swapchain_extent.width)
            .height(swapchain_extent.height)
            .layers(1)
            .build();
        framebuffers.push(logical_device.create_framebuffer(&framebuffer_create_info, None)?);
    }
    Ok(framebuffers)
}

pub unsafe fn create_command_buffers(
    logical_device: &ash::Device,
    command_pool: vk::CommandPool,
    image_views_count: u32,
) -> Result<Vec<vk::CommandBuffer>, vk::Result> {
    let command_buffer_allocate_info = vk::CommandBufferAllocateInfo::builder()
        .command_pool(command_pool)
        .level(vk::CommandBufferLevel::PRIMARY)
        .command_buffer_count(image_views_count);
    logical_device.allocate_command_buffers(&command_buffer_allocate_info)
}

pub unsafe fn reset_swapchain(
    vulkan: &Vulkan,
    renderer: &mut Renderer,
    shaders: &PipelineShaders,
) -> Result<(), vk::Result> {
    vulkan.device.logical.device_wait_idle()?;

    let surface_loader = ash::extensions::khr::Surface::new(&vulkan.entry, &vulkan.instance);
    let support_details = query_swap_chain_details(
        &surface_loader,
        vulkan.surface.clone(),
        vulkan.device.physical,
    )?;
    let (swapchain, _image_count) = create_swapchain(
        renderer.swapchain_loader.clone(),
        vulkan.surface,
        &support_details,
        &vulkan.device.queue_family_indices,
    )?;
    renderer.swapchain = swapchain;
    renderer.image_views = create_image_views(
        renderer.swapchain_loader.clone(),
        renderer.swapchain,
        &support_details,
        &vulkan.device.logical,
    )?;
    renderer.render_pass = create_render_pass(&support_details, &vulkan.device.logical)?;
    let extent_2d = support_details.choose_swap_extent();
    let graphics_pipeline_info = GraphicsPipelineInfo::new(
        vec![create_viewport(extent_2d)],
        vec![create_scissor(extent_2d)],
        vec![create_color_blend_attachment()],
    );
    let (graphics_pipeline, pipeline_cache) = create_graphics_pipeline(
        &vulkan.device.logical,
        shaders,
        graphics_pipeline_info,
        renderer.pipeline_layout,
        renderer.render_pass,
    )?;
    renderer.graphics_pipeline = graphics_pipeline[0];
    renderer.pipeline_cache = pipeline_cache;
    renderer.framebuffers = create_framebuffers(
        &vulkan.device.logical,
        &extent_2d,
        &renderer.image_views,
        renderer.render_pass,
    )?;
    renderer.command_buffers = create_command_buffers(
        &vulkan.device.logical,
        renderer.command_pool,
        renderer.image_views.len() as u32,
    )?;
    Ok(())
}
