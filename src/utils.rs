pub fn cstrings_as_pointers(strings: &[std::ffi::CString]) -> Vec<*const i8> {
    strings.iter().map(|string| string.as_ptr()).collect()
}

pub fn strs_as_cstrings(strings: &[&str]) -> Vec<std::ffi::CString> {
    strings
        .iter()
        .map(|string| std::ffi::CString::new(*string).unwrap())
        .collect()
}

pub fn strings_as_cstrings(strings: &[String]) -> Vec<std::ffi::CString> {
    strings
        .iter()
        .map(|string| std::ffi::CString::new(string.clone()).unwrap())
        .collect()
}
