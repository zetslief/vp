use ash;
use ash::extensions::khr;
use ash::vk;

use crate::{
    pipeline::{
        create_color_blend_attachment, create_graphics_pipeline, create_scissor, create_viewport,
        GraphicsPipelineInfo,
    },
    shaders::{create_descriptor_set_layout, PipelineShaders},
    swapchain::{
        create_command_buffers, create_framebuffers, create_image_views, create_render_pass,
        create_swapchain, query_swap_chain_details,
    },
    vulkan::Vulkan,
};

pub struct Renderer {
    pub swapchain_loader: ash::extensions::khr::Swapchain,
    pub swapchain: vk::SwapchainKHR,

    pub image_views: Vec<vk::ImageView>,

    pub render_pass: vk::RenderPass,

    pub pipeline_layout: vk::PipelineLayout,

    pub descriptor_set_layout: vk::DescriptorSetLayout,
    pub descriptor_pool: vk::DescriptorPool,
    pub descriptor_sets: Vec<vk::DescriptorSet>,

    pub pipeline_cache: vk::PipelineCache,
    pub quad_pipeline_cache: vk::PipelineCache,

    pub graphics_pipeline: vk::Pipeline,
    pub quad_pipeline: vk::Pipeline,

    pub framebuffers: Vec<vk::Framebuffer>,

    pub command_pool: vk::CommandPool,
    pub command_buffers: Vec<vk::CommandBuffer>,

    pub image_available_semaphore: vk::Semaphore,
    pub render_finished_semaphore: vk::Semaphore,
}

impl Renderer {
    pub unsafe fn new(
        vulkan: &Vulkan,
        shaders: &PipelineShaders,
        quad_shaders: &PipelineShaders,
    ) -> Result<Self, vk::Result> {
        let Vulkan {
            entry: _entry,
            instance,
            device,
            surface,
            surface_loader,
            swapchain_support_details,
        } = vulkan;

        let (descriptor_set_layout, _bindings) = create_descriptor_set_layout(&device.logical)?;
        let layouts = [descriptor_set_layout];

        let pipeline_layout_info = vk::PipelineLayoutCreateInfo::builder()
            .set_layouts(&layouts)
            .build();

        log::info!("Creating Pipeline Layout...");

        let pipeline_layout = device
            .logical
            .create_pipeline_layout(&pipeline_layout_info, None)?;

        let support_details = query_swap_chain_details(&surface_loader, *surface, device.physical)?;
        let swapchain_loader = khr::Swapchain::new(&instance, &device.logical);
        let (swapchain, image_count) = create_swapchain(
            swapchain_loader.clone(),
            *surface,
            &support_details,
            &device.queue_family_indices,
        )?;
        let image_views = create_image_views(
            swapchain_loader.clone(),
            swapchain,
            &support_details,
            &device.logical,
        )?;
        let render_pass = create_render_pass(&support_details, &device.logical)?;

        let descriptor_pool = create_descriptor_pool(&device.logical, image_count)?;
        let descriptor_sets = create_descriptor_sets(&device.logical, descriptor_pool, &layouts)?;

        let extent_2d = swapchain_support_details.choose_swap_extent();
        let graphics_pipeline_info = GraphicsPipelineInfo::new(
            vec![create_viewport(extent_2d)],
            vec![create_scissor(extent_2d)],
            vec![create_color_blend_attachment()],
        );
        let quad_graphics_pipeline_info = GraphicsPipelineInfo::new(
            vec![create_viewport(extent_2d)],
            vec![create_scissor(extent_2d)],
            vec![create_color_blend_attachment()],
        );
        log::info!("Render Pass created!");
        let (graphics_pipeline, pipeline_cache) = create_graphics_pipeline(
            &device.logical,
            shaders,
            graphics_pipeline_info,
            pipeline_layout,
            render_pass,
        )?;
        let graphics_pipeline = graphics_pipeline[0];
        log::info!("Graphics Pipeline created!");
        let (quad_pipeline, quad_pipeline_cache) = create_graphics_pipeline(
            &device.logical,
            quad_shaders,
            quad_graphics_pipeline_info,
            pipeline_layout,
            render_pass,
        )?;
        let quad_pipeline = quad_pipeline[0];
        log::info!("Graphics Pipeline created!");

        let swapchain_extent = support_details.choose_swap_extent();
        let framebuffers = create_framebuffers(
            &device.logical,
            &swapchain_extent,
            &image_views,
            render_pass,
        )?;

        let command_pool_info = vk::CommandPoolCreateInfo::builder()
            .queue_family_index(device.queue_family_indices.graphics_family)
            .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
            .build();

        let command_pool = device
            .logical
            .create_command_pool(&command_pool_info, None)?;

        let command_buffers =
            create_command_buffers(&device.logical, command_pool, image_views.len() as u32)?;

        let semaphore_info = vk::SemaphoreCreateInfo::builder().build();

        let image_available_semaphore = device.logical.create_semaphore(&semaphore_info, None)?;
        let render_finished_semaphore = device.logical.create_semaphore(&semaphore_info, None)?;

        Ok(Renderer {
            swapchain_loader,
            swapchain,
            image_views,
            render_pass,
            pipeline_layout,
            descriptor_set_layout,
            descriptor_pool,
            descriptor_sets,
            pipeline_cache,
            quad_pipeline_cache,
            graphics_pipeline,
            quad_pipeline,
            framebuffers,
            command_pool,
            command_buffers,
            image_available_semaphore,
            render_finished_semaphore,
        })
    }
}

unsafe fn create_descriptor_pool(
    device: &ash::Device,
    image_count: u32,
) -> Result<vk::DescriptorPool, vk::Result> {
    let pool_sizes = [vk::DescriptorPoolSize::builder()
        .ty(vk::DescriptorType::UNIFORM_BUFFER)
        .descriptor_count(image_count)
        .build()];
    let info = vk::DescriptorPoolCreateInfo::builder()
        .max_sets(image_count)
        .pool_sizes(&pool_sizes)
        .build();
    device.create_descriptor_pool(&info, None)
}

unsafe fn create_descriptor_sets(
    device: &ash::Device,
    pool: vk::DescriptorPool,
    set_layouts: &[vk::DescriptorSetLayout],
) -> Result<Vec<vk::DescriptorSet>, vk::Result> {
    let descriptor_set_allocate_info = vk::DescriptorSetAllocateInfo::builder()
        .descriptor_pool(pool)
        .set_layouts(&set_layouts)
        .build();
    device.allocate_descriptor_sets(&descriptor_set_allocate_info)
}
