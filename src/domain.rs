#[derive(Clone, Copy)]
#[repr(C)]
pub struct Vec2 {
    pub x: f32,
    pub y: f32,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub struct Vec4 {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}
pub type Color = Vec4;

#[derive(Clone, Copy)]
#[repr(C)]
pub struct Vertex {
    pub position: Vec2,
    pub color: Color,
}

#[derive(Clone)]
#[repr(C)]
pub struct UniformBuffer {
    pub time: f32,
    pub aspect_ratio: f32
}

pub fn colored_rect(left: f32, top: f32, width: f32, height: f32) -> [Vertex; 6] {
    let colors: [Color; 6] = [
        Color { r: 0.0, g: 0.0, b: 1.0, a: 1.0 },
        Color { r: 0.0, g: 1.0, b: 0.0, a: 1.0 },
        Color { r: 0.1, g: 0.1, b: 0.1, a: 1.0 },
        Color { r: 0.1, g: 0.1, b: 0.1, a: 1.0 },
        Color { r: 1.0, g: 0.0, b: 0.0, a: 1.0 },
        Color { r: 0.0, g: 0.0, b: 1.0, a: 1.0 },
    ];
    let rect = rect(left, top, width, height);
    rect.zip(colors)
        .map(|(position, color)| Vertex { position, color })
}

pub fn rect(left: f32, top: f32, width: f32, height: f32) -> [Vec2; 6] {
    let right = left + width;
    let bottom = top + height;
    [
        Vec2 { x: left, y: bottom },
        Vec2 { x: left, y: top },
        Vec2 { x: right, y: top },
        Vec2 { x: right, y: top },
        Vec2 { x: right, y: bottom },
        Vec2 { x: left, y: bottom },
    ]
}
