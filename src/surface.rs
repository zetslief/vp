use ash;
use ash::vk;
use log;
use raw_window_handle;
use raw_window_handle::{RawDisplayHandle, RawWindowHandle};

pub unsafe fn create_surface(
    instance: &ash::Instance,
    entry: &ash::Entry,
    raw_display_handle: RawDisplayHandle,
    raw_window_handle: RawWindowHandle,
) -> Result<vk::SurfaceKHR, vk::Result> {
    match (raw_display_handle, raw_window_handle) {
        #[cfg(target_os = "linux")]
        (RawDisplayHandle::Xlib(display), RawWindowHandle::Xlib(window)) => {
            log::info!("Found Xlib display handle: {:?}", display);
            log::info!("Found Xlib window handle: {:?}", window);
            get_xlib_surface(instance, entry, display, window)
        }
        #[cfg(target_os = "windows")]
        (RawDisplayHandle::Windows(_), RawWindowHandle::Windows(win32_handle)) => {
            log::info!("Found Win32 window handle: {:?}", win32_handle);
            get_win32_surface(instance, entry, win32_handle)
        }
        handle => {
            panic!("{:?} is not supported yet!", handle);
        }
    }
}

#[cfg(target_os = "linux")]
unsafe fn get_xlib_surface(
    instance: &ash::Instance,
    entry: &ash::Entry,
    display: raw_window_handle::XlibDisplayHandle,
    window: raw_window_handle::XlibWindowHandle,
) -> Result<vk::SurfaceKHR, vk::Result> {
    let xlib_surface_create_info = vk::XlibSurfaceCreateInfoKHR::builder()
        .dpy(display.display as *mut _)
        .window(window.window);
    log::debug!("Xlib surface create info created!");
    let surface_factory = ash::extensions::khr::XlibSurface::new(entry, instance);
    surface_factory.create_xlib_surface(&xlib_surface_create_info, None)
}

#[cfg(target_os = "windows")]
unsafe fn get_win32_surface(
    instance: &ash::Instance,
    entry: &ash::Entry,
    win32_handle: raw_window_handle::windows::WindowsHandle,
) -> Result<vk::SurfaceKHR, vk::Result> {
    let win32_surface_create_info = vk::Win32SurfaceCreateInfoKHR::builder()
        .hinstance(win32_handle.hinstance)
        .hwnd(win32_handle.hwnd);
    ash::extensions::khr::Win32Surface::new(entry, instance)
        .create_win32_surface(&win32_surface_create_info, None)
}
