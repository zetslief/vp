use std::ffi::CString;
use std::io;
use std::mem::size_of;
use std::path::Path;

use ash::vk;

use super::shader_compiler::{ShaderCompiler, Stage};
use crate::domain::{UniformBuffer, Vec2, Vertex};

const DEFAULT_VERTEX_SHADER: &'static str = "shaders/shader_vert.glsl";
const DEFAULT_VERTEX_SHADER_OUTPUT: &'static str = "shaders/shader_vert.spv";

const DEFAULT_FRAGMENT_SHADER: &'static str = "shaders/shader_frag.glsl";
const DEFAULT_FRAGMENT_SHADER_OUTPUT: &'static str = "shaders/shader_frag.spv";

const QUAD_VERTEX_SHADER: &'static str = "shaders/quad_vert.glsl";
const QUAD_VERTEX_SHADER_OUTPUT: &'static str = "shaders/quad_vert.spv";
const QUAD_FRAGMENT_SHADER: &'static str = "shaders/quad_frag.glsl";
const QUAD_FRAGMENT_SHADER_OUTPUT: &'static str = "shaders/quad_frag.spv";

pub struct PipelineShaders {
    pub entry: std::ffi::CString,
    pub shader_stages: Vec<vk::PipelineShaderStageCreateInfo>,
    pub bindings: Vec<vk::VertexInputBindingDescription>,
    pub attributes: Vec<vk::VertexInputAttributeDescription>,
    pub pipeline_vertex_input_state: vk::PipelineVertexInputStateCreateInfo,
}

pub struct VertexFragmentPair {
    pub vertex: vk::ShaderModule,
    pub fragment: vk::ShaderModule,
}

fn create_shaders(
    device: &ash::Device,
    compiler: &ShaderCompiler,
    vertex_input: &'static str,
    fragment_input: &'static str,
    vertex_output: &'static str,
    fragment_output: &'static str,
) -> Result<VertexFragmentPair, vk::Result> {
    let vertex_code = compile_shader(compiler, Stage::Vertex(vertex_input), vertex_output).unwrap();
    let fragment_code =
        compile_shader(compiler, Stage::Fragment(fragment_input), fragment_output).unwrap();
    create_shader_modules(device, &vertex_code, &fragment_code)
}

pub fn create_default_shaders(
    device: &ash::Device,
    compiler: &ShaderCompiler,
) -> Result<VertexFragmentPair, vk::Result> {
    create_shaders(
        device,
        compiler,
        DEFAULT_VERTEX_SHADER,
        DEFAULT_FRAGMENT_SHADER,
        DEFAULT_VERTEX_SHADER_OUTPUT,
        DEFAULT_FRAGMENT_SHADER_OUTPUT,
    )
}

pub fn create_quad_shaders(
    device: &ash::Device,
    compiler: &ShaderCompiler,
) -> Result<VertexFragmentPair, vk::Result> {
    create_shaders(
        device,
        compiler,
        QUAD_VERTEX_SHADER,
        QUAD_FRAGMENT_SHADER,
        QUAD_VERTEX_SHADER_OUTPUT,
        QUAD_FRAGMENT_SHADER_OUTPUT,
    )
}

pub fn compile_shader(
    compiler: &ShaderCompiler,
    input: Stage,
    output: &'static str,
) -> Result<Vec<u32>, io::Error> {
    let output = Path::new(output);
    if !compiler.compile(input, output) {
        panic!("Failed to compile vertex shader!");
    }
    read_shader(output)
}

pub fn create_shader_modules(
    device: &ash::Device,
    vertex_code: &[u32],
    fragment_code: &[u32],
) -> Result<VertexFragmentPair, vk::Result> {
    unsafe {
        let vertex_module_create_info = vk::ShaderModuleCreateInfo::builder().code(vertex_code);
        let vertex = device.create_shader_module(&vertex_module_create_info, None)?;
        let fragment_module_create_info = vk::ShaderModuleCreateInfo::builder().code(fragment_code);
        let fragment = device.create_shader_module(&fragment_module_create_info, None)?;
        Ok(VertexFragmentPair { vertex, fragment })
    }
}

fn read_shader(path: &std::path::Path) -> std::io::Result<Vec<u32>> {
    let mut file = std::fs::File::open(path)?;
    ash::util::read_spv(&mut file)
}

pub fn create_defatul_pipeline_shaders(
    shaders: &VertexFragmentPair,
    entry_point: CString,
) -> PipelineShaders {
    let vertex_shader_stage_create_info = vk::PipelineShaderStageCreateInfo::builder()
        .stage(vk::ShaderStageFlags::VERTEX)
        .module(shaders.vertex)
        .name(&entry_point)
        .build();

    let fragment_shader_stage_create_info = vk::PipelineShaderStageCreateInfo::builder()
        .stage(vk::ShaderStageFlags::FRAGMENT)
        .module(shaders.fragment)
        .name(&entry_point)
        .build();

    let bindings = vec![vk::VertexInputBindingDescription::builder()
        .binding(0)
        .stride(size_of::<Vertex>() as u32)
        .input_rate(vk::VertexInputRate::VERTEX)
        .build()];
    let attributes = vec![
        vk::VertexInputAttributeDescription::builder()
            .binding(0)
            .location(0)
            .format(vk::Format::R32G32_SFLOAT)
            .offset(memoffset::offset_of!(Vertex, position) as u32)
            .build(),
        vk::VertexInputAttributeDescription::builder()
            .binding(0)
            .location(1)
            .format(vk::Format::R32G32B32A32_SFLOAT)
            .offset(memoffset::offset_of!(Vertex, color) as u32)
            .build(),
    ];
    let pipeline_vertex_input_state = vk::PipelineVertexInputStateCreateInfo::builder()
        .vertex_binding_descriptions(&bindings)
        .vertex_attribute_descriptions(&attributes)
        .build();

    let shader_stages = vec![vertex_shader_stage_create_info, fragment_shader_stage_create_info];
    PipelineShaders {
        entry: entry_point,
        shader_stages,
        bindings,
        attributes,
        pipeline_vertex_input_state,
    }
}

pub fn create_quad_pipeline_shaders(
    shaders: &VertexFragmentPair,
    entry_point: CString,
) -> PipelineShaders {
    let vertex_shader_stage_create_info = vk::PipelineShaderStageCreateInfo::builder()
        .stage(vk::ShaderStageFlags::VERTEX)
        .module(shaders.vertex)
        .name(&entry_point)
        .build();

    let fragment_shader_stage_create_info = vk::PipelineShaderStageCreateInfo::builder()
        .stage(vk::ShaderStageFlags::FRAGMENT)
        .module(shaders.fragment)
        .name(&entry_point)
        .build();
    let bindings = vec![vk::VertexInputBindingDescription::builder()
        .binding(0)
        .stride(size_of::<Vec2>() as u32)
        .input_rate(vk::VertexInputRate::VERTEX)
        .build()];
    let attributes = vec![vk::VertexInputAttributeDescription::builder()
        .binding(0)
        .location(0)
        .format(vk::Format::R32G32_SFLOAT)
        .offset(memoffset::offset_of!(Vec2, x) as u32)
        .build()];
    let pipeline_vertex_input_state = vk::PipelineVertexInputStateCreateInfo::builder()
        .vertex_binding_descriptions(&bindings)
        .vertex_attribute_descriptions(&attributes)
        .build();

    let shader_stages = vec![vertex_shader_stage_create_info, fragment_shader_stage_create_info];
    PipelineShaders {
        entry: entry_point,
        shader_stages,
        bindings,
        attributes,
        pipeline_vertex_input_state,
    }
}

pub unsafe fn create_descriptor_set_layout(
    device: &ash::Device,
) -> Result<(vk::DescriptorSetLayout, Vec<vk::DescriptorSetLayoutBinding>), vk::Result> {
    use vk::ShaderStageFlags;
    let bindings = vec![vk::DescriptorSetLayoutBinding::builder()
        .binding(0)
        .descriptor_type(vk::DescriptorType::UNIFORM_BUFFER)
        .descriptor_count(1)
        .stage_flags(ShaderStageFlags::VERTEX | ShaderStageFlags::FRAGMENT)
        .build()];
    let layout_info = vk::DescriptorSetLayoutCreateInfo::builder()
        .bindings(&bindings)
        .build();
    device
        .create_descriptor_set_layout(&layout_info, None)
        .map(|layout| (layout, bindings))
}

pub unsafe fn configure_descriptor_sets(
    device: &ash::Device,
    descriptor_sets: &[vk::DescriptorSet],
    uniform_buffers: &[crate::buffer::Buffer],
) {
    for (descriptor_set, uniform_buffer) in descriptor_sets.iter().zip(uniform_buffers) {
        let buffer_info = [vk::DescriptorBufferInfo::builder()
            .buffer(uniform_buffer.buffer)
            .offset(0)
            .range(size_of::<UniformBuffer>() as vk::DeviceSize)
            .build()];
        let descriptor_write = vk::WriteDescriptorSet::builder()
            .dst_set(*descriptor_set)
            .dst_binding(0)
            .dst_array_element(0)
            .descriptor_type(vk::DescriptorType::UNIFORM_BUFFER)
            .buffer_info(&buffer_info)
            .build();
        device.update_descriptor_sets(&[descriptor_write], &[])
    }
}
