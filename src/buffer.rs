use ash::vk;

pub struct Buffer {
    pub buffer: vk::Buffer,
    pub device_memory: vk::DeviceMemory,
    pub size: vk::DeviceSize,
}

impl Buffer {
    pub unsafe fn new(
        device: &ash::Device,
        size: vk::DeviceSize,
        usage: vk::BufferUsageFlags,
        sharing_mode: vk::SharingMode,
        memory_flags: vk::MemoryPropertyFlags,
        memory_properties: vk::PhysicalDeviceMemoryProperties,
    ) -> Result<Self, vk::Result> {
        let buffer_create_info = vk::BufferCreateInfo::builder()
            .size(size)
            .usage(usage)
            .sharing_mode(sharing_mode);
        let buffer = device.create_buffer(&buffer_create_info, None)?;
        let memory_requirements = device.get_buffer_memory_requirements(buffer);
        let memory_type_index =
            filter_memory_types(memory_properties, memory_requirements, memory_flags)
                .expect("Valid memory type has been found");
        let memory_allocate_info = vk::MemoryAllocateInfo::builder()
            .allocation_size(memory_requirements.size)
            .memory_type_index(memory_type_index);
        let device_memory = device.allocate_memory(&memory_allocate_info, None)?;
        device.bind_buffer_memory(buffer, device_memory, 0)?;
        Ok(Self {
            buffer,
            device_memory,
            size: memory_allocate_info.allocation_size,
        })
    }
}

pub unsafe fn copy<T>(device: &ash::Device, src: &[T], dst: &Buffer) -> Result<(), vk::Result>
where
    T: Sized,
{
    let data = device.map_memory(
        dst.device_memory,
        0,
        dst.size,
        vk::MemoryMapFlags::default(),
    )?;
    let data = data.cast::<T>();
    std::ptr::copy_nonoverlapping(src.as_ptr(), data, src.len());
    device.unmap_memory(dst.device_memory);
    Ok(())
}

pub unsafe fn create_vertex_buffer<T: Sized>(
    device: &ash::Device,
    memory_properties: vk::PhysicalDeviceMemoryProperties,
    data: &[T],
) -> Result<Buffer, vk::Result> {
    let buffer = Buffer::new(
        device,
        (data.len() * std::mem::size_of::<T>()) as vk::DeviceSize,
        vk::BufferUsageFlags::VERTEX_BUFFER,
        vk::SharingMode::EXCLUSIVE,
        vk::MemoryPropertyFlags::DEVICE_LOCAL | vk::MemoryPropertyFlags::HOST_COHERENT,
        memory_properties,
    )?;
    copy(&device, data, &buffer)?;
    Ok(buffer)
}

pub unsafe fn create_uniform_buffer<T: Sized>(
    device: &ash::Device,
    memory_properties: vk::PhysicalDeviceMemoryProperties,
    data: &[T],
) -> Result<Buffer, vk::Result> {
    let buffer = Buffer::new(
        device,
        (data.len() * std::mem::size_of::<T>()) as vk::DeviceSize,
        vk::BufferUsageFlags::UNIFORM_BUFFER,
        vk::SharingMode::EXCLUSIVE,
        vk::MemoryPropertyFlags::DEVICE_LOCAL | vk::MemoryPropertyFlags::HOST_COHERENT,
        memory_properties,
    )?;
    copy(&device, data, &buffer)?;
    Ok(buffer)
}

unsafe fn filter_memory_types(
    properties: vk::PhysicalDeviceMemoryProperties,
    requirements: vk::MemoryRequirements,
    flags: vk::MemoryPropertyFlags,
) -> Option<u32> {
    properties.memory_types[..properties.memory_type_count as usize]
        .iter()
        .enumerate()
        .find(|(index, memory_type)| {
            (1 << index) & requirements.memory_type_bits != 0
                && memory_type.property_flags & flags == flags
        })
        .map(|(index, _)| index as u32)
}
