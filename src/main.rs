#![feature(vec_into_raw_parts)]
#![feature(array_zip)]

pub mod buffer;
pub mod device;
pub mod domain;
pub mod draw;
pub mod logger;
pub mod pipeline;
pub mod renderer;
pub mod shader_compiler;
pub mod shaders;
pub mod surface;
pub mod swapchain;
pub mod utils;
pub mod vulkan;

use std::ffi::CString;

use ash::vk;
use log;
use winit::event_loop::EventLoop;
use winit::window::{Window, WindowBuilder};

use raw_window_handle::{HasRawDisplayHandle, HasRawWindowHandle};

use buffer::create_vertex_buffer;

const VALIDATION_LAYERS: [&str; 1] = ["VK_LAYER_KHRONOS_validation"];
//const VALIDATION_LAYERS: [&str; 2] = ["VK_LAYER_KHRONOS_validation", "VK_LAYER_LUNARG_api_dump"];
const LOGGER: logger::Logger = logger::Logger {};
const LOG_LEVEL: log::LevelFilter = log::LevelFilter::Info;

fn main() {
    log::set_logger(&LOGGER).expect("Failed to set the logger!");
    log::trace!("Logger is enabled!");
    LOGGER.init(LOG_LEVEL);

    let event_loop = winit::event_loop::EventLoop::new();
    let window = create_window(&event_loop);
    run(event_loop, window)
}

fn create_window(event_loop: &EventLoop<()>) -> Window {
    WindowBuilder::new()
        .build(&event_loop)
        .expect("Failed to create a window!")
}

fn run(event_loop: EventLoop<()>, window: Window) -> ! {
    let mut vulkan = unsafe {
        vulkan::Vulkan::new(
            window.raw_display_handle(),
            window.raw_window_handle(),
            &VALIDATION_LAYERS,
        )
        .expect("Failed to create vulkan!")
    };

    let shader_compiler = shader_compiler::ShaderCompiler::new(std::path::PathBuf::from("glslc"));
    let default_shaders = shaders::create_default_shaders(&vulkan.device.logical, &shader_compiler)
        .expect("Failed to create default shaders!");
    let quad_shaders = shaders::create_quad_shaders(&vulkan.device.logical, &shader_compiler)
        .expect("Filed to create quad shaders!");

    let shaders =
        shaders::create_defatul_pipeline_shaders(&default_shaders, CString::new("main").unwrap());
    let quad_pipeline_shaders =
        shaders::create_quad_pipeline_shaders(&quad_shaders, CString::new("main").unwrap());

    let mut renderer = unsafe {
        renderer::Renderer::new(&vulkan, &shaders, &quad_pipeline_shaders)
            .expect("Failed to create renderer")
    };
    let vertexes = domain::colored_rect(-0.5, -0.5, 1.0, 1.0);
    let vertex_buffer = unsafe {
        create_vertex_buffer(
            &vulkan.device.logical,
            vulkan.device.memory_properties,
            &vertexes,
        )
        .unwrap()
    };
    let quad_vertexes = domain::rect(-0.9, -0.9, 0.1, 0.1);
    let quad_vertex_buffer = unsafe {
        create_vertex_buffer(
            &vulkan.device.logical,
            vulkan.device.memory_properties,
            &quad_vertexes,
        )
        .unwrap()
    };

    let uniform_buffer = domain::UniformBuffer { time: 0.0, aspect_ratio: 1.0 };
    let mut uniform_buffer_data = [uniform_buffer];
    let uniform_buffers: Vec<buffer::Buffer> = unsafe {
        (0..renderer.image_views.len())
            .map(|_| {
                buffer::create_uniform_buffer(
                    &vulkan.device.logical,
                    vulkan.device.memory_properties,
                    &uniform_buffer_data,
                )
                .unwrap()
            })
            .collect()
    };
    unsafe {
        shaders::configure_descriptor_sets(
            &vulkan.device.logical,
            &renderer.descriptor_sets,
            &uniform_buffers,
        );
    }

    let graphics_queue = unsafe {
        vulkan
            .device
            .logical
            .get_device_queue(vulkan.device.queue_family_indices.graphics_family, 0)
    };
    let mut skip_events = false;
    let mut left_position = -1.0;
    event_loop.run(move |event, _, control_flow| {
        control_flow.set_wait();
        match event {
            winit::event::Event::WindowEvent {
                event: winit::event::WindowEvent::CloseRequested,
                window_id,
            } if window_id == window.id() => {
                unsafe {
                    vulkan
                        .device
                        .logical
                        .device_wait_idle()
                        .expect("Failed to wait device idle!");
                    log::warn!("Uniform buffers: {}", uniform_buffers.len());
                    vulkan.cleanup(
                        &mut renderer,
                        vec![
                            default_shaders.vertex,
                            default_shaders.fragment,
                            quad_shaders.vertex,
                            quad_shaders.fragment,
                        ],
                        vec![
                            (vertex_buffer.buffer, vertex_buffer.device_memory),
                            (quad_vertex_buffer.buffer, quad_vertex_buffer.device_memory),
                            (uniform_buffers[0].buffer, uniform_buffers[0].device_memory),
                            (uniform_buffers[1].buffer, uniform_buffers[1].device_memory),
                            (uniform_buffers[2].buffer, uniform_buffers[2].device_memory),
                            (uniform_buffers[3].buffer, uniform_buffers[3].device_memory),
                            (uniform_buffers[4].buffer, uniform_buffers[4].device_memory),
                        ],
                    );
                    log::warn!("Cleanup finished");
                };
                control_flow.set_exit();
                skip_events = true;
            }
            winit::event::Event::MainEventsCleared => {
                log::trace!("Update frame {event:?}");
                if skip_events {
                    log::warn!("Skip events after finish: {event:#?}");
                    return;
                }
                unsafe {
                    let quad_vertexes = domain::rect(left_position, -0.9, 0.1, 0.1);
                    buffer::copy(&vulkan.device.logical, &quad_vertexes, &quad_vertex_buffer)
                        .unwrap();
                };
                left_position += 0.01;
                if left_position > 0.9 {
                    left_position = -1.0;
                }
                let (image_index, _suboptimal) = unsafe {
                    match renderer.swapchain_loader.acquire_next_image(
                        renderer.swapchain,
                        u64::MAX,
                        renderer.image_available_semaphore,
                        vk::Fence::null(),
                    ) {
                        Ok(result) => result,
                        Err(vk::Result::ERROR_OUT_OF_DATE_KHR) => {
                            log::warn!("RESET SWAPCHAIN");
                            swapchain::reset_swapchain(&vulkan, &mut renderer, &shaders)
                                .expect("Failed to reset swapchain");
                            return;
                        }
                        _ => panic!("Failed to acquire next image!"),
                    }
                };
                let semaphores = [renderer.image_available_semaphore];
                let wait_stages = [vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT];
                let signal_semaphores = [renderer.render_finished_semaphore];

                let extent_2d = vulkan.swapchain_support_details.choose_swap_extent();
                let buffers = [vertex_buffer.buffer];
                let quad_buffers = [quad_vertex_buffer.buffer];
                unsafe {
                    draw::draw(
                        &vulkan.device.logical,
                        renderer.framebuffers[image_index as usize],
                        renderer.command_pool,
                        renderer.command_buffers[image_index as usize],
                        renderer.render_pass,
                        renderer.pipeline_layout,
                        &renderer.descriptor_sets,
                        renderer.graphics_pipeline.clone(),
                        renderer.quad_pipeline.clone(),
                        &buffers,
                        vertexes.len(),
                        &quad_buffers,
                        quad_vertexes.len(),
                        extent_2d,
                    )
                    .expect("Draw is executed seccessfully.");
                };

                let command_buffer = renderer
                    .command_buffers
                    .get(image_index as usize)
                    .expect("Failed to get command buffer!");
                let buffers = [*command_buffer];
                update_uniform_buffer(&mut uniform_buffer_data[0], extent_2d);
                unsafe {
                    log::warn!("Uniform time: {}", uniform_buffer_data[0].time);
                    buffer::copy(
                        &vulkan.device.logical,
                        &uniform_buffer_data,
                        &uniform_buffers[image_index as usize],
                    )
                    .unwrap();
                };

                let submit_info = vk::SubmitInfo::builder()
                    .command_buffers(&buffers)
                    .wait_semaphores(&semaphores)
                    .signal_semaphores(&signal_semaphores)
                    .wait_dst_stage_mask(&wait_stages)
                    .build();
                let submits = [submit_info];
                unsafe {
                    vulkan
                        .device
                        .logical
                        .queue_submit(graphics_queue, &submits, vk::Fence::null())
                        .expect("Failed to queue submit!");
                };
                let swapchains = [renderer.swapchain];
                let image_indices = [image_index];
                let present_info = vk::PresentInfoKHR::builder()
                    .wait_semaphores(&signal_semaphores)
                    .swapchains(&swapchains)
                    .image_indices(&image_indices)
                    .build();

                unsafe {
                    match renderer
                        .swapchain_loader
                        .queue_present(graphics_queue, &present_info)
                    {
                        Ok(_) => log::trace!("Queue present ok!"),
                        Err(vk::Result::ERROR_OUT_OF_DATE_KHR | vk::Result::SUBOPTIMAL_KHR) => {
                            log::warn!("Queue present error: reset swapchain!");
                            swapchain::reset_swapchain(&vulkan, &mut renderer, &shaders)
                                .expect("Failed to reset swapchain");
                        }
                        Err(error) => {
                            panic!("Failed to present queue {error}")
                        }
                    };
                    vulkan
                        .device
                        .logical
                        .queue_wait_idle(graphics_queue)
                        .expect("Failed to wait queue idle!");
                }
            }
            _event => {}
        };
    });
}

fn update_uniform_buffer(uniform_buffer: &mut domain::UniformBuffer, extend: vk::Extent2D) {
    uniform_buffer.time += 1.0;
    uniform_buffer.aspect_ratio = extend.height as f32 / extend.width as f32;
    log::warn!("Updated time: {}", uniform_buffer.time);
    log::warn!("Update aspect_ratio: {}", uniform_buffer.aspect_ratio);
}
