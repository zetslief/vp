#version 450

layout(binding = 0) uniform UniformBufferObject {
    float time;
    float aspect_ratio;
} ubo;

layout(location = 0) out vec4 outColor;

void main() {
    outColor = vec4(0.2, mod(ubo.time / 200.0, 2.0), 0.1, 1.0);
}
