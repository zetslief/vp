#version 450

layout(binding = 0) uniform UniformBufferObject {
    float time;
    float aspect_ratio;
} ubo;
layout(location = 0) in vec2 inPosition;

void main() {
    gl_Position = vec4(inPosition.x * ubo.aspect_ratio, inPosition.y, 0.0, 1.0);
}
